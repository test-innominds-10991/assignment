package com.innominds.Question1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class EvenNumber {
	
	ArrayList<Integer> l1=new ArrayList<Integer>();	
	ArrayList<Integer> l2=new ArrayList<Integer>();	
	public void saveEvenNumbers(int n)
	{
		for(int i=0;i<n;i++)
		{
			if(i%2==0)
			{
				l1.add(i);
			}
		}
	}
	public ArrayList<Integer> saveEvenNumbers()
	{
		return l1;
	}
	public ArrayList<Integer> printEvenNumbers()
	{
		for(int i=1;i<l1.size();i++)
		{
			int b =(l1.get(i))*2;
			l2.add(b);
		}
		Iterator r=(Iterator) l2.iterator();
		while(r.hasNext())
		{
			System.out.print(r.next()+"\t");
		}
		return l2;
	}
	public int printEvenNumber(int n)
	{
		for(int i=0;i<l2.size();i++)
		{
			if(n==l2.get(i))
			{
				return n;
			}
			else
			{
				return 0;
			}
		}
		return n;
	}
}

