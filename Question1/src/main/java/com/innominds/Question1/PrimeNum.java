package com.innominds.Question1;

import java.util.Scanner;

public class PrimeNum {
	public static void main(String[] args)
	{
		int[] myArray = {1,2,3,4,5,6,7,8,9};
		Scanner n = new Scanner (System.in);
		System.out.println("Enter the elements in array;");
		for(int i=0; i<5; i++)
		{
			myArray[i] = n.nextInt();
		}
		for(int i=0;i<myArray.length; i++)
		{
			boolean isPrime = true;
			
			for(int j=2; j<i; j++)
			{
				if(i%j==0)
				{
					isPrime = false;
					break;
				}
			}
			if(isPrime)
			System.out.println(i +"are the prime numbers in the array");		
		}
	}
}
