package com.innominds.Question1;

import java.util.ArrayList;
import java.util.Collections;

public class EmployeeObject
{
	 private int eid;
	    private String ename;
	    private String department;

	   public String geteName() {
	        return ename;
	    }

	   public void seteName(String ename) {
	        this.ename = ename;
	    }

	   public EmployeeObject(int eid, String ename, String department) {
	        super();
	        this.eid = eid;
	        this.ename = ename;
	        this.department = department;
	    }

	   public static ArrayList<EmployeeObject > sortEmployee(ArrayList<EmployeeObject > empList) {
	        Collections.sort(empList, (a, b) -> a.geteName().compareTo(b.geteName()));
	        ;
	        return empList;
	    }

	   @Override
	    public String toString() {
	        return "Employee [id=" + eid + ", name=" + ename + ", department=" + department + "]";
	    }

	   @Override
	    public boolean equals(Object obj) {

	       return ((Integer) ((EmployeeObject ) (obj)).eid).equals(this.eid);
	    }

	   public static void main(String[] args) {
	        ArrayList<EmployeeObject > empList = new ArrayList<>();
	       empList.add(new EmployeeObject (189, "Anudeep", "UI"));
	        empList.add(new EmployeeObject (234, "Sumith", "PYTHON"));
	        System.out.println(sortEmployee(empList));
	    }
}
