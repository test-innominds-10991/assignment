package com.crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Insert
{
	public static void main(String[] args)
	{
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try(Connection con = DriverManager.getConnection("jdbc:mysql://@localhost:3306/assign","root","root");
				Statement st = con.createStatement();)
		{
			String query = "INSERT INTO EMPLOYEE(EMPID,EMPNAME,DESIGNATION,SALARY) VALUES(024,'anudeep','techie',30000)";
			
			int count = st.executeUpdate(query);
			if(count==0)
			{
				System.out.println("Record not inserted");
			}
			else
			{
				System.out.println("Record is inserted");
			}
	}
		catch(SQLException se)
		{
			System.out.println(se.getMessage());
			se.printStackTrace();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		}
}
