package com.innominds.Question1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Question2Test {

	 Question2 question2 = new Question2();
	    @Test
	    void testDigit() {
	assertEquals("Digit", question2.checkCharacter('2'));
	    }

	    @Test
	    void testUpper1() {

	char  ch = 'A';

	assertEquals("UpperCase", question2.checkCharacter(ch));

	    }

	    @Test

	    void testUpper2() {

	char  ch = 'A';

	assertNotEquals("LowerCase", question2.checkCharacter(ch));

	    }
	    
	    @Test

	    void testLower() {

	char  ch = 'a';

	assertEquals("LowerCase", question2.checkCharacter(ch));

	    }

	}


