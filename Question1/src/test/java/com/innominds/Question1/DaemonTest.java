package com.innominds.Question1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DaemonTest {

	@Test
	public void daemonThread() 
	{
		Daemon d=new Daemon();
		d.setDaemon(true);
		d.start();
		assertEquals(true, d.isDaemon());
	}
	
	@Test
	public void notDaemonThread() 
	{
		Daemon d=new Daemon();
		d.start();
		assertNotEquals(true, d.isDaemon());
	}
	
	@Test
	public void DaemonThread() 
	{
		Daemon d=new Daemon();
		Daemon d1=new Daemon();
		d.start();
		d1.setDaemon(true);
		d1.start();
		assertEquals(true, d1.isDaemon());
		assertEquals(false, d.isDaemon());
	}


}
