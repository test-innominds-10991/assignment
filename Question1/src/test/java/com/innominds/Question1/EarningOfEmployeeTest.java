package com.innominds.Question1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EarningOfEmployeeTest
{
	EarningOfEmployee e = new EarningOfEmployee();

	@Test
	public void salaryLessThan50000() 
	{
		assertEquals(0, e.taxPaidByEmployee(40000));
	}

	@Test
	public void salaryLessThan60000() 
	{
		assertEquals(200, e.taxPaidByEmployee(52000));
	}
	
	@Test
	public void salaryLessThan150000() 
	{
		assertEquals(22000, e.taxPaidByEmployee(120000));
	}
	
	@Test
	public void salaryOther() 
	{
		assertEquals(64000, e.taxPaidByEmployee(300000));
	}

}
