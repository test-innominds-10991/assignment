package com.innominds.Question1;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.assertEquals;


import org.junit.jupiter.api.Test;

class LambdaTest {

	@Test
	void test() {
		 Lambda l=new Lambda();
		   assertEquals(30, l.add());
	   }
	   @Test
	   public void normalAdd()
	   {
		   Lambda l=new Lambda();
		   assertEquals(40,l.addNormal(20, 20));
		   
	   }

}
